#!/bin/bash

# Vérifier le nombre de paramètres
if [ "$#" -ne 4 ]; then
    echo "Usage: $0 VMTargetStart numberStart numberEnd inventoryFile"
    exit 1
fi

# Récupérer les paramètres
vm_target_start=$1
number_start=$2
number_end=$3
inventory_file=$4

# Supprimer le fichier d'inventaire existant et créer un nouveau
rm -f $inventory_file
touch $inventory_file

# Boucler pour obtenir l'adresse IP de chaque VM
for i in $(seq $number_start $number_end); do
    # Obtenir l'adresse IP de l'interface de la VM
    ip=$(virsh domifaddr $vm_target_start$i | grep ipv4 | awk '{print $4}' | cut -d'/' -f1)
    
    # Ajouter le groupe et l'adresse IP au fichier d'inventaire
    echo "[$vm_target_start$i]" >> $inventory_file
    echo "$ip" >> $inventory_file
done

# Ajouter le groupe "all:children" à la fin du fichier d'inventaire
echo -e "\n[all:children]" >> $inventory_file
for i in $(seq $number_start $number_end); do
    echo "sprin$i" >> $inventory_file
done


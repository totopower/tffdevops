### copier la vm debian11bis de KVM et la nommer mysonet1

### changer en ip fixe 192.168.122.201
-> sudo nano /etc/network/interfaces
ligne -> iface enp1s0 inet dhcp
par ->
iface enp1s0 inet static
  address 192.168.122.201
  netmask 255.255.255.0
  gateway 192.168.122.1
  dns-nameservers 1.1.1.1 8.8.8.8 8.8.4.4

/!!! Sur Raspberry !!!\
-> sudo nano /etc/dhcpcd.conf
-> tout à la fin
interface eth0
static ip_address=192.168.122.201/24
static routers=192.168.122.1
static domain_name_servers=1.1.1.1 8.8.8.8 8.8.4.4

### installer mysql pour créer correctement l'user mysql et les groupes
sudo apt install gnupg git
sudo apt install mariadb-server

### installer apache2
sudo apt install apache2
sudo mkdir /var/www/mysonet
sudo chown -R $USER:$USER /var/www/mysonet
sudo chmod -R 755 /var/www/mysonet

### créer une page d'accueil de test
nano /var/www/mysonet/index.html
-> C'est nul

### activer le site
sudo nano /etc/apache2/sites-available/mysonet.conf
->
<VirtualHost *:80>
    ServerAdmin tonPseudo@localhost
    ServerName mysonet
    ServerAlias www.mysonet.online
    DocumentRoot /var/www/mysonet
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

sudo a2ensite mysonet.conf
sudo a2dissite 000-default.conf
sudo systemctl reload apache2

### tester sur le navigateur web 192.168.122.201

### installer PHP
sudo apt-get install ca-certificates apt-transport-https software-properties-common wget curl lsb-release
curl -sSL https://packages.sury.org/php/README.txt | sudo bash -x
sudo apt update
sudo apt install php8.1
sudo apt-get install libapache2-mod-php8.1
sudo systemctl restart apache2

### tester si php fonctionne bien
mv /var/www/mysonet/index.html /var/www/mysonet/index.php
nano /var/www/mysonet/index.php
->
<!DOCTYPE html>
<html>
<head>
    <title>Ping IP</title>
</head>
<body>
    <form method="POST">
        <input type="text" name="ip" required placeholder="Entrer une adresse IP">
        <input type="submit" name="submit" value="Ping">
    </form>
</body>
</html>

<?php
if (isset($_POST['submit'])) {
    $ip = $_POST['ip'];

    // Assurez-vous que l'adresse IP est valide.
    if (!filter_var($ip, FILTER_VALIDATE_IP) === false) {
        // Utilisez la commande ping.
        $output = shell_exec('ping -c 1 ' . escapeshellarg($ip));

        // Vérifiez si la commande ping a réussi.
        if (strpos($output, '1 received') !== false) {
            echo "Ping OK";
        } else {
            echo "Error";
        }
    } else {
        echo("$ip n'est pas une adresse IP valide");
    }
}
?>
aller sur le navigateur web 192.168.122.201 et tenter un ping via le formulaire

### création de l'utilisateur inspectorsonet
sudo groupadd -g 19133 inspectorsonet
sudo useradd -u 19133 -g inspectorsonet -m -s /bin/bash inspectorsonet
sudo passwd inspectorsonet

### tester une connexion ssh avec mysonet de sprin vers inspectorsonet@192.168.122.201

### créer un ID MySonet avec inspectorsonet
su - inspectorsonet
nano idmysonet
-> écrireUnId <- c'est comme un mot de passe

### ajouter la clé publique de mysonet
mkdir .ssh
nano .ssh/authorized_keys
-> exmple :
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBr5qpaTfStpIReW6mqo8Jccjkl5yjpyDFIaW8ZSIi0p/rnhWge4K2E/Akrm63wxLxF/Iqlj4wLPxSTdW6hWaWis6453qUM1Jhvhz3IDsbvYqlHzEEfcRStsxmCleE4lgXInb2nWPdb0jP6Pjnoiz2Rf0sdZnujSlD869pNGrBNAAW4mZibVSHsKAbZzZ5eYwpIlqk1hYxWlbX59x5+paJQtUQAFUjkTXJ6eLf1EBmvF/FTdgP+HU3AtSU3staLsjSC8xQW7R4jpKdl9VC4TF37m1bEUVSezZ5a/1SelldoK/+zW9AdXkaFqoXTh4VsPiiHTRvYcvPIiy3k8gCVXUaXe2EHkTd8YWno4gflrCBS3KuIj/UWJbLjWfD+fUtYBfwHT4TOlOKcO4ZtowDUojn/TqAumxg0P+YeHpwjJFaEi0ra4T6geJDsiq/Y1/tfNeIj8QWmuX9xItzRmoBAaehvJoxQz0tFrRQLwQCKsRHTE5GHVNK3c6ar7+XSvYsaqs= mysonet@debian

exit

/!!!\ NE PAS FAIRE LA SUITE

### installer Docker :
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo usermod -aG docker tonPseudo

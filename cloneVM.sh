#!/bin/bash

# Vérifier le nombre de paramètres
if [ "$#" -ne 4 ]; then
    echo "Usage: $0 VMSource VMTargetStart numberStart numberEnd"
    exit 1
fi

# Récupérer les paramètres
vm_source=$1
vm_target_start=$2
number_start=$3
number_end=$4

# Boucler pour cloner les VMs
for i in $(seq $number_start $number_end); do
    echo "Cloning VM to $vm_target_start$i..."
    virt-clone --original $vm_source --name $vm_target_start$i --auto-clone
done


#!/bin/bash

# Vérifier le nombre de paramètres
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 VMTargetStart numberStart numberEnd"
    exit 1
fi

# Récupérer les paramètres
vm_target_start=$1
number_start=$2
number_end=$3

# Boucler pour démarrer les VMs
for i in $(seq $number_start $number_end); do
    echo "Starting VM $vm_target_start$i..."
    virsh start $vm_target_start$i
done


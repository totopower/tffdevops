#!/bin/bash

# Vérifier le nombre de paramètres
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 VMTargetStop numberStart numberEnd"
    exit 1
fi

# Récupérer les paramètres
vm_target_stop=$1
number_start=$2
number_end=$3

# Boucler pour stopper les VMs
for i in $(seq $number_start $number_end); do
    echo "Stopping VM $vm_target_stop$i..."
    virsh shutdown $vm_target_stop$i
done

